const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: [
    'vuetify',
  ],
  devServer: {
    proxy: 'https://vue2sample.s3.ap-northeast-1.amazonaws.com/',
  }
});
