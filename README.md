# Vue2 & Vuetify & TypeScript

## まずは環境調査

nodejs v18.11.0 になってることを確認

```sh
% node -v 
v18.11.0
%
```

カレントディレクトリの`node_modules/.bin`にパスが通ってる事を確認。

```shell
% echo $PATH | sed -r 's/.*(\.\/node_modules\/\.bin).*/\1/g'
./node_modules/.bin
%
```
## yarn で vue/cli

vueのコマンドラインツール`@vue/cli`をインストール.  
2022/10/22現在では`vue@2.7.13`がインストールされる　

```shell
% yarn add -D @vue/cli
yarn add v1.22.10
info No lockfile found.
[1/4] 🔍  Resolving packages...
...

✨  Done in 25.55s.
% ls
node_modules    package.json    yarn.lock
```

## Vue v2プロジェクトの作成

```shell
% vue create .
Vue CLI v5.0.8
? Generate project in current directory? (Y/n) Y

? Please pick a preset: 
  Default ([Vue 3] babel, eslint) 
  Default ([Vue 2] babel, eslint) 
❯ Manually select features 

? Check the features needed for your project: (Press <space> to select, <a> to toggle all, <i> to invert selection, and <enter> to 
proceed)
 ◉ Babel
❯◉ TypeScript
 ◯ Progressive Web App (PWA) Support
 ◯ Router
 ◯ Vuex
 ◯ CSS Pre-processors
 ◉ Linter / Formatter
 ◯ Unit Testing
 ◯ E2E Testing

? Choose a version of Vue.js that you want to start the project with 
  3.x 
❯ 2.x 

? Use class-style component syntax? (Y/n) Y

? Use Babel alongside TypeScript (required for modern mode, auto-detected polyfills, transpiling JSX)? (Y/n) n

? Pick a linter / formatter config: 
  ESLint with error prevention only 
❯ ESLint + Airbnb config 
  ESLint + Standard config 
  ESLint + Prettier 

? Pick additional lint features: (Press <space> to select, <a> to toggle all, <i> to invert selection, and <enter> to proceed)
❯◉ Lint on save
 ◯ Lint and fix on commit

? Where do you prefer placing config for Babel, ESLint, etc.? (Use arrow keys)
❯ In dedicated config files 
  In package.json 

? Save this as a preset for future projects? (y/N) N
```

プロジェクト生成が始まります。

```shell
✨  Creating project in /Users/tetsuya/git/tmp/vue2-sample2.
🗃  Initializing git repository...
⚙️  Installing CLI plugins. This might take a while...

yarn install v1.22.10
[1/4] 🔍  Resolving packages...

...

⚓  Running completion hooks...

📄  Generating README.md...

🎉  Successfully created project vue2-sample2.
👉  Get started with the following commands:

 $ yarn serve

% ls  
README.md       node_modules    package.json    public          src             tsconfig.json   yarn.lock
```

## Vue の動作確認

```shell
% yarn serve

 DONE  Compiled successfully in 2946ms                                                                                           10:33:19


  App running at:
  - Local:   http://localhost:8080/ 
  - Network: http://192.168.96.30:8080/

  Note that the development build is not optimized.
  To create a production build, run yarn build.

No issues found.
```

` - Local: http://localhost:8080/`をブラウザで開くとVueの画面が表示される。

![Vueの画面](imgs/2022-10-23%2010.35.21.png)

## Vuetifyのインストール

```shell
% vue add vuetify

📦  Installing vue-cli-plugin-vuetify...

yarn add v1.22.10
[1/4] 🔍  Resolving packages...

...

? Choose a preset: (Use arrow keys)
  Vuetify 2 - Configure Vue CLI (advanced) 
❯ Vuetify 2 - Vue CLI (recommended) 
  Vuetify 2 - Prototype (rapid development) 
  Vuetify 3 - Vite (preview) 
  Vuetify 3 - Vue CLI (preview) 

🚀  Invoking generator for vue-cli-plugin-vuetify...
📦  Installing additional dependencies...

yarn install v1.22.10

...

success Saved lockfile.
✨  Done in 5.44s.
⚓  Running completion hooks...

✔  Successfully invoked generator for plugin: vue-cli-plugin-vuetify
 vuetify  Discord community: https://community.vuetifyjs.com
 vuetify  Github: https://github.com/vuetifyjs/vuetify
 vuetify  Support Vuetify: https://github.com/sponsors/johnleider

% 
```

```shell
 % yarn serve
yarn run v1.22.10
$ vue-cli-service serve
 INFO  Starting development server...

...


 DONE  Compiled successfully in 11584ms                                                                                          10:41:41


  App running at:
  - Local:   http://localhost:8080/ 
  - Network: http://192.168.96.30:8080/

  Note that the development build is not optimized.
  To create a production build, run yarn build.

No issues found.
```

ブラウザリロードすると今度はVuetifyのデザインで表示される。

![vuetify](imgs/2022-10-23%2010.42.09.png)

## 現状確認とgitコミット

この時点での `package.json`

```json
{
  "name": "",
  "version": "",
  "private": true,
  "scripts": {
    "serve": "vue-cli-service serve",
    "build": "vue-cli-service build",
    "lint": "vue-cli-service lint"
  },
  "dependencies": {
    "vue": "^2.6.14",
    "vue-class-component": "^7.2.3",
    "vue-property-decorator": "^9.1.2",
    "vuetify": "^2.6.0"
  },
  "devDependencies": {
    "@typescript-eslint/eslint-plugin": "^5.4.0",
    "@typescript-eslint/parser": "^5.4.0",
    "@vue/cli": "^5.0.8",
    "@vue/cli-plugin-eslint": "~5.0.0",
    "@vue/cli-plugin-typescript": "~5.0.0",
    "@vue/cli-service": "~5.0.0",
    "@vue/eslint-config-airbnb": "^6.0.0",
    "@vue/eslint-config-typescript": "^9.1.0",
    "eslint": "^7.32.0",
    "eslint-plugin-import": "^2.25.3",
    "eslint-plugin-vue": "^8.0.3",
    "eslint-plugin-vuejs-accessibility": "^1.1.0",
    "sass": "~1.32.0",
    "sass-loader": "^10.0.0",
    "typescript": "~4.5.5",
    "vue-cli-plugin-vuetify": "~2.5.8",
    "vue-template-compiler": "^2.6.14",
    "vuetify-loader": "^1.7.0"
  },
  "_id": "@",
  "readme": "ERROR: No README data found!"
}

```

この時点で`git`設定されている。

```shell
% git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   package.json
        modified:   public/index.html
        modified:   src/App.vue
        modified:   src/components/HelloWorld.vue
        modified:   src/main.ts
        modified:   yarn.lock

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        src/assets/logo.svg
        src/plugins/
        src/shims-vuetify.d.ts
        vue.config.js

no changes added to commit (use "git add" and/or "git commit -a")
% 
```

あとの作業のためにブランチを作ってコミットしておく。

```shell
 % git checkout -b develop
Switched to a new branch 'develop'
% 
```

念のため`.gitignore`確認。ファイルがこうなってること。

```gitignore
.DS_Store
node_modules
/dist


# local env files
.env.local
.env.*.local

# Log files
npm-debug.log*
yarn-debug.log*
yarn-error.log*
pnpm-debug.log*

# Editor directories and files
.idea
.vscode
*.suo
*.ntvs*
*.njsproj
*.sln
*.sw?

```

ステージして

```shell
% git add *
The following paths are ignored by one of your .gitignore files:
node_modules
hint: Use -f if you really want to add them.
hint: Turn this message off by running
hint: "git config advice.addIgnoredFile false"
% 
```

コメント付けてコミット

```
% git commit -m 'vuetifyの適用まで'
[develop 42e5b9a] vuetifyの適用まで
 10 files changed, 341 insertions(+), 98 deletions(-)
 rewrite src/App.vue (76%)
 create mode 100644 src/assets/logo.svg
 rewrite src/components/HelloWorld.vue (97%)
 create mode 100644 src/plugins/vuetify.ts
 create mode 100644 src/shims-vuetify.d.ts
 create mode 100644 vue.config.js
% 
```

## 環境整備

この辺でVSCodeを再起動しておく  
TypeScriptとTSLintをローカルのnode_modules以下のものを使うため再起動が必要。

`tsconfig.json`に`vueCmpilerOptions`を追加

```json
{
  "vueCompilerOptions": {
    "target": 2.7
  },
  "compilerOptions": {
    "target": "es5",
    "module": "esnext",
    "strict": true,
    "jsx": "preserve",
    "importHelpers": true,
    "moduleResolution": "node",
    "experimentalDecorators": true,
    "skipLibCheck": true,
    "esModuleInterop": true,
    "allowSyntheticDefaultImports": true,
    "forceConsistentCasingInFileNames": true,
    "useDefineForClassFields": true,
    "sourceMap": true,
    "baseUrl": ".",
    
    "types": [
      "webpack-env"
    ],
    "paths": {
      "@/*": [
        "src/*"
      ]
    },
    "lib": [
      "esnext",
      "dom",
      "dom.iterable",
      "scripthost"
    ]
  },
  "include": [
    "src/**/*.ts",
    "src/**/*.tsx",
    "src/**/*.vue",
    "tests/**/*.ts",
    "tests/**/*.tsx"
  ],
  "exclude": [
    "node_modules"
  ]
}
```


## クラススタイルコンポーネントに変更する

`Vue.extend`を使うVue2の標準スタイルのコンポーネントは開発しづらいので`ClassComponent`とAnnotationによるクラススタイルコンポーネントを適用する。

`components/HelloWorld.vue`を編集

95行目から152行目まで全部コメントアウト
```vue
<script lang="ts">
// import Vue from 'vue';

// export default Vue.extend({

// ...

// });
</script>
```

`vue-property-decorator`から`Component`と`Vue`をimport

```ts
<script lang="ts">
import { Component, Vue } from 'vue-property-decorator';
```

クラススタイルでコンポーネントを定義

```ts
import { Component, Vue } from 'vue-property-decorator';

@Component
export default class HelloWorld extends Vue {

}
```

画面上の各種一覧のための型を定義

```ts
type ListItem = {
  text: string
  href: string
}
```

コメントアウトで消えてしまっている一覧要素をクラスに追加

```ts
import { Component, Vue } from 'vue-property-decorator';

type ListItem = {
  text: string
  href: string
}

@Component
export default class HelloWorld extends Vue {
  ecosystem: ListItem[] = [];

  importantLinks: ListItem[] = [];

  whatsNext: ListItem[] = [];
}

```


そろそろ`yarn serve`してみる

```shell
% yarn serve

 DONE  Compiled successfully in 10278ms                                                                                                                                   12:01:44


  App running at:
  - Local:   http://localhost:8080/ 
  - Network: http://192.168.96.30:8080/

  Note that the development build is not optimized.
  To create a production build, run yarn build.

No issues found.
```

一覧要素が無くなっているのがわかる。

![noList](imgs/2022-10-23%2012.02.05.png)


`ecosystem`の一覧要素をコピペして復活させると、Ecosystemの項目のリンクが復活することが分かる。

```ts
@Component
export default class HelloWorld extends Vue {
  ecosystem: ListItem[] = [
    {
      text: 'vuetify-loader',
      href: 'https://github.com/vuetifyjs/vuetify-loader',
    },
    {
      text: 'github',
      href: 'https://github.com/vuetifyjs/vuetify',
    },
    {
      text: 'awesome-vuetify',
      href: 'https://github.com/vuetifyjs/awesome-vuetify',
    },
  ];

  importantLinks: ListItem[] = [];

  whatsNext: ListItem[] = [];
}

```

![ecosystem](imgs/2022-10-23%2012.03.49.png)

同様に、全部復活させておく。

復活させたらコメントアウト領域は削除。

```ts
<script lang="ts">
import { Component, Vue } from 'vue-property-decorator';

type ListItem = {
  text: string
  href: string
}

@Component
export default class HelloWorld extends Vue {
  ecosystem: ListItem[] = [
    {
      text: 'vuetify-loader',
      href: 'https://github.com/vuetifyjs/vuetify-loader',
    },
    {
      text: 'github',
      href: 'https://github.com/vuetifyjs/vuetify',
    },
    {
      text: 'awesome-vuetify',
      href: 'https://github.com/vuetifyjs/awesome-vuetify',
    },
  ];

  importantLinks: ListItem[] = [
    {
      text: 'Documentation',
      href: 'https://vuetifyjs.com',
    },
    {
      text: 'Chat',
      href: 'https://community.vuetifyjs.com',
    },
    {
      text: 'Made with Vuetify',
      href: 'https://madewithvuejs.com/vuetify',
    },
    {
      text: 'Twitter',
      href: 'https://twitter.com/vuetifyjs',
    },
    {
      text: 'Articles',
      href: 'https://medium.com/vuetify',
    },
  ];

  whatsNext: ListItem[] = [
    {
      text: 'Explore components',
      href: 'https://vuetifyjs.com/components/api-explorer',
    },
    {
      text: 'Select a layout',
      href: 'https://vuetifyjs.com/getting-started/pre-made-layouts',
    },
    {
      text: 'Frequently Asked Questions',
      href: 'https://vuetifyjs.com/getting-started/frequently-asked-questions',
    },
  ];
}
</script>
```

## コンポーネントを追加してみる

`HellowWorld.vue`をコピーして`Page1.vue`を作る

![Page1](imgs/2022-10-23%2012.07.26.png)

`Welcome to Vuetify`の文字を`Welcome to Page1`に書き換える
```html
      <v-col class="mb-4">
        <h1 class="display-2 font-weight-bold mb-3">
          Welcome to Page1
        </h1>

```

クラスの名前も同様に書き換える。

```ts
@Component
export default class Page1 extends Vue {
  ecosystem: ListItem[] = [
    {

```

HelloWorldからPage1を利用するために`Component`アノテーションに追記する。

`HelloWorld.vue`を編集
```ts
import { Component, Vue } from 'vue-property-decorator';
import Page1 from './Page1.vue';

type ListItem = {
  text: string
  href: string
}

@Component({
  components: {
    Page1,
  },
})
export default class HelloWorld extends Vue { 
```

画面の下あたりに`<Page1/>`を追加してみる

```html
      </v-col>
    </v-row>
    <Page1/>
  </v-container>
</template>

```

すると、画面上に追加される。

![追加](imgs/2022-10-23%2012.13.43.png)

## Vue-routerによるURLルーティング

Page1は画面のパーツと言うより画面そのものなので、
`/page1`とかでPage1を表示させたい。

[vue-router](https://router.vuejs.org/)を追加。
最新バージョンの4系はvue2に対応していないので3.5.1を指定。

```shell
% yarn add vue-router@3.5.1
yarn add v1.22.10
info No lockfile found.
[1/4] 🔍  Resolving packages...
[2/4] 🚚  Fetching packages...
[3/4] 🔗  Linking dependencies...
[4/4] 🔨  Building fresh packages...
success Saved lockfile.
success Saved 1 new dependency.
info Direct dependencies
└─ vue-router@3.5.1
info All dependencies
└─ vue-router@3.5.1
✨  Done in 0.43s.
% 
```

`src/router/index.ts`を作成

![router/index](imgs/2022-10-23%2012.18.41.png)

ひとまず`/`をHelloWorldにしておいて、`src/router/index.ts`を作成

```ts
import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import HelloWorld from '@/components/HelloWorld.vue';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  { path: '/', component: HelloWorld },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;

```

`src/App.vue`では`HelloWorld`ではなく`router`を表示させるように変更。

変更前

```ts

    <v-main>
      <HelloWorld/>
    </v-main>
  </v-app>
</template>

<script lang="ts">
import Vue from 'vue';
import HelloWorld from './components/HelloWorld.vue';

export default Vue.extend({
  name: 'App',

  components: {
    HelloWorld,
  },

  data: () => ({
    //
  }),
});
</script>
```

変更後

```ts

    <v-main>
      <router-view/>
    </v-main>
  </v-app>
</template>

<script lang="ts">
import Vue from 'vue';
// import HelloWorld from './components/HelloWorld.vue';

export default Vue.extend({
  name: 'App',

  // components: {
  //   HelloWorld,
  // },

  data: () => ({
    //
  }),
});
</script>

```

`main.ts`で`view-router`をVueに追加。

```ts
import Vue from 'vue';
import router from '@/router';
import App from './App.vue';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');

```

`yarn serve`で確認。

元通り表示されている。

![router](imgs/2022-10-23%2012.13.43.png)

`src/router/index.ts`のルート定義に`Page1`を追加すると

```ts
import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import HelloWorld from '@/components/HelloWorld.vue';
import Page1 from '@/components/Page1.vue';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  { path: '/', component: HelloWorld },
  { path: '/page1', component: Page1 },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;

```

`/page1`にアクセスするとPage1コンポーネントが表示されるようになる。

![page1](imgs/2022-10-23%2012.31.05.png)


## HTMLの要素を置いてみる

画面をリンクで接続したいので、`HelloWorld.vue`にあった`<Page1/>`コンポーネントはコメントアウト。

```html
      </v-col>
    </v-row>
<!--
    <Page1/>
-->
</v-container>
</template>
```

What's next?の上に領域を作る。とりあえずダミーテキストを置いて確認。

```html
      </v-col>

      <v-col
        class="mb-5"
        cols="12"
      >
        <v-row justify="center">
          dummy
        </v-row>
      </v-col>

      <v-col
        class="mb-5"
        cols="12"
      >
        <h2 class="headline font-weight-bold mb-3">
          What's next?
        </h2>
```

![dummy](imgs/2022-10-23%2012.39.01.png)

ダミー部分を`router-link`コンポーネントで置き換えると、`/page1`へのリンクになる。

```html
        <v-row justify="center">
          <router-link
            to="/page1"
          >Page1</router-link>
        </v-row>
```

![link1](imgs/2022-10-23%2012.41.47.png)

クリックするとPage1に遷移します。

逆も同様に作る。

![page1tohello](imgs/2022-10-23%2012.43.05.png)

これでrouterを使ったリンクで各画面に遷移できるようになる。


## セレクトボックス

`Page1`の`HelloWorld`リンクの下にvuetifyの標準コンポーネント`v-select`を置いてみる

```html
      <v-col
        class="mb-5"
        cols="12"
      >
        <v-row justify="center">
          <router-link
            to="/"
          >HelloWorld</router-link>
        </v-row>

        <v-row justify="center">
          <v-col class="col-6">
            <v-select
              class="col-1"
              label="セレクトボックス"
            ></v-select>
          </v-col>
        </v-row>
      </v-col>
```

![select](imgs/2022-10-23%2012.50.42.png)

値はまだ無い

![nodata](imgs/2022-10-23%2012.51.15.png)

`selectItems`プロパティを表示するように指定しておいて

```html
            <v-select
              class="col-1"
              label="セレクトボックス"
              :items="selectItems"
              item-text="text"
            ></v-select>
```

クラスにプロパティを追加

```ts
@Component
export default class Page1 extends Vue {
  selectItems: {text: string, value: string}[] = [
    { text: '選択肢1', value: '1' },
  ];

  ecosystem: ListItem[] = [

```

選択肢が現れる

![selectOk](imgs/2022-10-23%2012.55.41.png)

### リモートから取得した値を画面に表示させたい

`https://vue2sample.s3.ap-northeast-1.amazonaws.com/watsnew.json`を取得してセレクトボックスの選択肢として表示させてみる。

まずはlocalhostからのCORS対応のためにproxyを明示する。

[CORSについてのわりとわかりやすい説明（外部）](https://dev.classmethod.jp/articles/about-cors/)

```js
const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: [
    'vuetify',
  ],
  devServer: {
    proxy: 'https://vue2sample.s3.ap-northeast-1.amazonaws.com/',
  }
});
```

通信用ライブラリ`axios`をインストール

```shell
% yarn add axios
yarn add v1.22.10
warning ../package.json: No license field
[1/4] 🔍  Resolving packages...
...

success Saved lockfile.
success Saved 6 new dependencies.
info Direct dependencies
└─ axios@1.1.3
info All dependencies
├─ asynckit@0.4.0
├─ axios@1.1.3
├─ combined-stream@1.0.8
├─ delayed-stream@1.0.0
├─ form-data@4.0.0
└─ proxy-from-env@1.1.0
✨  Done in 3.90s.
% 
```

`src/services`ディレクトリを作り、その中に`remote.ts`を作る。

![remoteService](imgs/2022-10-23%2013.03.55.png)

```ts
import axios from 'axios';

type JsonItem = {
  text: string
  href: string
};

const getItems = async (): Promise<JsonItem[]> => {
  const res = await axios.get('/watsnew.json');
  console.log(res.data);
  return res.data;
};

export default getItems;

```

Page1のselectItemsを置き換える。

コンポーネントのmount時に取得させたいので`mounted`メソッドで代入する。

```ts
import getItems from '@/services/remote';
import { Component, Vue } from 'vue-property-decorator';

type ListItem = {
  text: string
  href: string
}

@Component
export default class Page1 extends Vue {
  // selectItems: {text: string, value: string}[] = [
  //   { text: '選択肢1', value: '1' },
  // ];
  async mounted() {
    this.selectItems = await getItems();
  }

  selectItems: ListItem[] = [];
```

※`yarn serve`は再起動させておく

セレクトボックスに、リモートサーバのjsonの内容が表示させる。

![selectBYremote](imgs/2022-10-23%2013.17.01.png)


とりあえずここまで。  
コミットしておく。

```shell
% git add *
The following paths are ignored by one of your .gitignore files:
node_modules
hint: Use -f if you really want to add them.
hint: Turn this message off by running
hint: "git config advice.addIgnoredFile false"
tetsuya@mbp2018 vue2-sample2 % git commit -m 'サーバーからjson取ってきてselect'
[develop 1eaab0f] サーバーからjson取ってきてselect
 10 files changed, 408 insertions(+), 71 deletions(-)
 create mode 100644 src/components/Page1.vue
 create mode 100644 src/router/index.ts
 create mode 100644 src/service/remote.ts
%
``` 