import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import HelloWorld from '@/components/HelloWorld.vue';
import Page1 from '@/components/Page1.vue';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  { path: '/', component: HelloWorld },
  { path: '/page1', component: Page1 },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
