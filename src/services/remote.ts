import axios from 'axios';

type JsonItem = {
  text: string
  href: string
};

const getItems = async (): Promise<JsonItem[]> => {
  const res = await axios.get('/watsnew.json');
  console.log(res.data);
  return res.data;
};

export default getItems;
